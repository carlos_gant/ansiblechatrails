# README

Práctica de aplicación de chat en rails

# Prerequisitos

Puesto que la aplicación usa un servidor mongodb deberá haber uno configurado.
No he utilizado credenciales y debe conectar con una instalación limpia
de mongodb.

# INSTALACIÓN

Para instalar y ejecutar la aplicación siga los siguientes pasos:

## Clona y prepara el repo

```
git clone git@bitbucket.org:carlos_gant/ansiblechatrails.git
cd ansiblechatrails
yarn install
bin/setup
```

## Ejecuta la app

```
bundle exec foreman start
```

## Ejecuta los tests

```
bundle exec rspec
```

Hay tests de integración con capybara, por lo que pueden tardar un poco
en ejecutarse.

# FRAMEWORKS Y LIBRERÍAS UTILIZADAS:

## Frontend

* vuejs v2
* vuex
* semantic-ui-vue https://semantic-ui-vue.github.io/#/
* scss
* starwars-names https://www.npmjs.com/package/starwars-names

## Backend

* rails v5
* mongoid
* actioncable
* webpacker
