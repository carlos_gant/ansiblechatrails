import Vue from 'vue'
import VueRouter from 'vue-router'

import Landpage from './pages/Landpage'
import About from './pages/About'
import Help from './pages/Help'
import Lobby from './pages/Lobby'

Vue.use(VueRouter)

const routes = [
  { name: 'root', path: '/', component: Landpage},
  { name: 'about', path: '/about', component: About},
  { name: 'help', path: '/help', component: Help},
  { name: 'lobby', path: '/lobby', component: Lobby},
  { name: 'lobby_room', path: '/lobby/:room_id', component: Lobby}
]

const router = new VueRouter({routes})

const debug_router = false

if(debug_router){
  router.beforeEach((to, from, next) => {
    console.log("redirecting to", to)
    next()
  })
}

export default router
