import { normalizeUser } from '../normalize'

export default class UserListChannel {

  constructor(consumer, store){
    this.consumer = consumer
    this.store = store
    this.channel = null
  }

  connect(room_id){
    if(this.channel){
      this.consumer.subscriptions.remove(this.channel)
      this.channel = null
    }

    const subscriber = {channel: 'UserListChannel', room_id: room_id}
    const events = {
      received: (data) => {
        const users = data.map(normalizeUser)
        this.store.commit('setRoomUserList', users)
      }
    }

    this.channel = this.consumer.subscriptions.create(subscriber, events)
  }
}

