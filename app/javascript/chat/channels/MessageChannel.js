import { normalizeMessage } from '../normalize'

export default class MessageChannel {

  constructor(consumer, store){
    this.consumer = consumer
    this.store = store
    this.channel = null
  }

  connect(room_id){
    if(this.channel){
      this.consumer.subscriptions.remove(this.channel)
      this.channel = null
    }

    const subscriber = {channel: 'MessageChannel', room_id: room_id}
    const events = {
      received: (data) => {
        const message = normalizeMessage(data)
        this.store.commit('appendMessage', message)
      }
    }

    this.channel = this.consumer.subscriptions.create(subscriber, events)
  }
}

