export default {
  user: state => state.user,
  logged: state => state.user.id && true,
  rooms: state => state.rooms,
  activeRoom: state => state.active_room,
  isActiveRoom: state => id => state.active_room && state.active_room.id == id,
  getUserName: state => user_id => state.active_room.users[user_id].name
}
