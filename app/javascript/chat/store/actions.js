import server from '../server'

export default {
  login({commit}, username){
    return server.login(username)
      .then(({user}) => commit('setUser', user))
  },
  logout({commit}){
    commit('logout')
  },
  loadRooms({commit}){
    return server.rooms().then(({rooms}) => {
      commit('setRoomList', rooms)
    })
  },
  createRoom({commit, state}, name){
    return server.createRoom(state.user.id, name).then(({room}) => {
      commit('addRoom', room)
    })
  },
  enterRoom({commit, state}, room_id){
    return server.enterRoom(state.user.id, room_id)
      .then(({room}) => {
        commit('setActiveRoom', room)
      })
  },
  sendMessage({state}, text){
    const user_id = state.user.id
    const room_id = state.active_room.id

    return server.sendMessage(user_id, room_id, text)
  },
}
