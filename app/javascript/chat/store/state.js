function getStoredUser(){
  var id = null
  var name = null

  const encoded_user = sessionStorage.getItem('user')

  if(encoded_user){
    const decoded_user = JSON.parse(encoded_user)
    if(decoded_user){
      id = decoded_user.id
      name = decoded_user.name
    }
  }

  return {id, name}
}

export function getInitialState(){
  return {
    user: getStoredUser(),
    rooms: [],
    active_room: null
  }
}

export default {
  user: getStoredUser(),
  rooms: [],
  active_room: null
}
