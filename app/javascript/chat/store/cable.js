import ActionCable from 'actioncable'
import MessageChannel from '../channels/MessageChannel'
import UserListChannel from '../channels/UserListChannel'

export default function(store){

  const consumer = ActionCable.createConsumer()
  const message_channel = new MessageChannel(consumer, store)
  const user_list_channel = new UserListChannel(consumer, store)

  const listened_mutations = {
    setActiveRoom(mutation, state){
      const room = mutation.payload
      message_channel.connect(room.id)
      user_list_channel.connect(room.id)
    },
  }

  store.subscribe((mutation, state) => {
    if(mutation.type in listened_mutations){
      listened_mutations[mutation.type](mutation, state)
    }
  })
}
