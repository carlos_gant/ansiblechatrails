import {getInitialState} from './state'

export default {
  setUser(state, user) {
    state.user.id = user.id
    state.user.name = user.name
    sessionStorage.setItem('user', JSON.stringify(state.user))
  },
  logout(state) {
    sessionStorage.removeItem('user')
    Object.assign(state, getInitialState())
    state.user.id = null
    state.user.name = null
  },
  setRoomList(state, rooms) {
    state.rooms = rooms
  },
  addRoom(state, room) {
    state.rooms.push(room)
  },
  setActiveRoom(state, room) {
    state.active_room = room
  },
  setRoomUserList(state, users){
    state.active_room.users = users
  },
  appendMessage(state, message){
    state.active_room.messages.push(message)
  },
}
