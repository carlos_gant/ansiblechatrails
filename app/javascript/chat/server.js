import axios from 'axios'
import {normalizeUser, normalizeRoom, normalizeMessage} from './normalize'

const meta = document.querySelector('meta[name=csrf-token]')
const csrf_token = meta ? meta.content : ''

const client = axios.create({
  headers: {
    'X-CSRF-Token': csrf_token
  }
})

export default {
  login(username){
    return client.post('/users', {name: username}).then(r => {
      return {user: normalizeUser(r.data.user)}
    })
  },
  rooms(){
    return client.get('/rooms').then(r => {
      return {rooms: r.data.rooms.map(normalizeRoom)}
    })
  },
  createRoom(user_id, name){
    return client.post('/rooms', {user_id, name}).then(r => {
      return {room: normalizeRoom(r.data.room)}
    })
  },
  enterRoom(user_id, room_id){
    return client.post('/enter_room', {user_id, room_id}).then(r => {
      const room = normalizeRoom(r.data.room)
      room.users = r.data.users.map(normalizeUser).reduce((rv, user) => {
        rv[user.id] = user
        return rv
      }, {})
      room.messages = r.data.messages.map(normalizeMessage)
      return {room}
    })
  },
  sendMessage(user_id, room_id, text){
    return client.post('/messages', {user_id, room_id, text})
  },
}
