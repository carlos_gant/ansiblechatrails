/**
 * Normaliza los datos un usuario
 */
export function normalizeUser(user){
  const id = user['_id']['$oid']
  const name = user['name']

  return {id, name}
}

export function normalizeRoom(room){
  const id = room['_id']['$oid']
  const name = room['name']
  const num_users = 0

  return {id, name, num_users}
}

export function normalizeMessage(message){
  const id = message['_id']['$oid']
  const text = message['text']
  const created_at = message['created_at']
  const user_name = message['user']['name']

  return {id, text, created_at, user_name}
}
