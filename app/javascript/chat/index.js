import Vue from 'vue'
import router from './routes'
import Chat from './chat.vue'
import store from './store'
import SuiVue from 'semantic-ui-vue';

// asi puedo importar el cable
// import Cable from 'cable'

// Formato de fechas
import VueFilterDateFormat from 'vue-filter-date-format'

// styles
import 'semantic-ui-css/semantic.min.css';
import './chat.scss'

Vue.use(SuiVue)
Vue.use(VueFilterDateFormat)

document.addEventListener('DOMContentLoaded', () => {
  const el = document.body.appendChild(document.createElement('chat'));
  const app = new Vue({
    el,
    router,
    store,
    render: h => h(Chat)
  })
})
