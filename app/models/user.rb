# frozen_string_literal: true

# Chat users
class User
  include Mongoid::Document
  include Mongoid::Timestamps

  USERNAME_PATTERN = /\A\w[\wáéíóúñüÁÉÍÓÚÑÜ ]+\z/

  # fields
  field :name, type: String

  # relations
  belongs_to :room, optional: true
  has_many :messages

  # indexes
  index({ name: 1 }, unique: true)

  # validations
  validates :name,
            presence: true,
            length: { minimum: 3, maximum: 50 },
            uniqueness: true,
            format: { with: USERNAME_PATTERN }

  def self.enter_room(user_id, room_id)
    user = User.find(user_id)
    old_room = user.room_id

    user.room_id = room_id
    user.save!

    notify_user_list(old_room) if old_room
    notify_user_list(user.room_id)
  end

  def self.notify_user_list(room_id)
    UserListChannel.broadcast_to(room_id, User.where(room_id: room_id))
  end
end
