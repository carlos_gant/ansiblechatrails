# frozen_string_literal: true

# Chat rooms
class Room
  include Mongoid::Document
  include Mongoid::Timestamps

  # fields
  field :name, type: String

  # relations
  has_many :users
  has_many :messages

  # indexes
  index({ name: 1 }, unique: true)

  # validations
  validates :name,
            presence: true,
            length: { minimum: 3, maximum: 50 },
            uniqueness: true,
            format: { with: /\A\w[\w ]+\z/ }
end
