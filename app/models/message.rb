# frozen_string_literal: true

# Representa un mensaje de un usuario en una sala
class Message
  include Mongoid::Document
  include Mongoid::Timestamps::Created

  # fields
  field :text, type: String

  # relations
  belongs_to :room, index: true
  belongs_to :user, index: true

  # validations
  validates :text,
            presence: true

  validates :room, presence: true
  validates :user, presence: true

  default_scope -> { order_by(created_at: 'desc').includes(:user) }

  def self.last_messages(room_id)
    where(room_id: room_id).limit(configured_history)
  end

  def self.configured_history
    Rails.configuration.x.chat.num_messages_when_entering_room
  end
end
