# frozen_string_literal: true

# Gestiona los mensajes enviados
class MessagesController < ApplicationController
  def create
    message = Message.create!(message_params)

    MessageChannel.broadcast_to(
      message.room,
      message.as_json(include: { user: { only: :name } })
    )
  end

  private

  def message_params
    params.permit(:user_id, :room_id, :text)
  end
end
