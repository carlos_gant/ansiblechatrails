# frozen_string_literal: true

# Gestiona la pagina de inicio y carga el frontend
class ChatController < ApplicationController
  def index; end
end
