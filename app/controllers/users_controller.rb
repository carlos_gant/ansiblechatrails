# frozen_string_literal: true

# Gestiona los usuarios
class UsersController < ApplicationController
  def create
    user = User.find_or_create_by(room_params)
    render json: { user: user }
  end

  def enter_room
    User.enter_room(params[:user_id], params[:room_id])
    redirect_to room_path(params[:room_id])
  end

  private

  def room_params
    params.permit(:name)
  end
end
