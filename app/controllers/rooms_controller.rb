# frozen_string_literal: true

# Gestiona las salas del chat
class RoomsController < ApplicationController
  def index
    rooms = Room.all
    render json: { rooms: rooms }
  end

  def create
    room = Room.create(room_params)
    render json: { room: room }
  end

  def show
    room = Room.includes(:users).find(params[:id])
    messages = Message.last_messages(room.id)
    render json: {
      room: room,
      users: room.users,
      messages: messages.as_json(include: { user: { only: :name } })
    }
  end

  private

  def room_params
    params.permit(:name)
  end
end
