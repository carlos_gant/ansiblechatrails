# frozen_string_literal: true

Rails.application.routes.draw do
  # rooms
  resources :rooms, only: %i[index create show]

  # messages
  resources :messages, only: %i[create]

  # users
  resources :users, only: %i[create]
  post '/enter_room', to: 'users#enter_room'

  # Root
  root to: 'chat#index'
end
