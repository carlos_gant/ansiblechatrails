# frozen_string_literal: true

require 'rails_helper'

feature 'About page', type: :feature do
  describe 'when user visits /#/', js: true do
    before do
      destroy_user_session
      visit root_path
    end
    it 'can see the about link' do
      expect(page).to have_link('Acerca de')
    end
    it 'should go to the about page when clicks the about button' do
      click_link('Acerca de')
      within('h2') do
        expect(page).to have_content('Acerca de')
      end
    end
  end
  describe 'when user visits /#/about directly', js: true do
    before do
      destroy_user_session
      visit '/#/about'
    end
    it 'show display the about page' do
      expect(page).to have_selector('h2', text: 'Acerca de')
      expect(page).to have_link('Volver')
    end
    it 'should go back when clicks the back button' do
      click_link('Volver')
      expect(page).to have_content('Elige un nombre')
    end
  end
end
