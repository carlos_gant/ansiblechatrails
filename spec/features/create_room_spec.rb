# frozen_string_literal: true

require 'rails_helper'

feature 'Create room', type: :feature do
  context 'users visits /#/lobby', js: true do
    before do
      destroy_user_session
      login_with 'MyUser'
    end
    it 'sees the lobby' do
      expect(page).to have_css('.lobby')
    end
    it 'sees no room' do
      expect(page).to have_selector('a.room', count: 0)
    end
    it 'sees the new room link' do
      expect(page).to have_link('Nueva sala')
    end
    it 'creates the new room' do
      click_link('Nueva sala')

      expect(page).to have_content('Crea tu sala')
      expect(page).to have_content('pulsa Aceptar')
      expect(page).to have_field('room_name')

      fill_in('room_name', with: 'MyRoom1')
      click_button('Aceptar')

      expect(page).to have_selector('a.room', count: 1)
      expect(page).to have_content('MyRoom1')
    end
  end
end
