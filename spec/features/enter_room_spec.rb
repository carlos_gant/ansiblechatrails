# frozen_string_literal: true

require 'rails_helper'

feature 'Enter room', type: :feature, js: true do
  background do
    room = Room.create!(name: 'Room1')

    user = User.create!(name: 'MyUser1', room: room)
    other_user = User.create!(name: 'OtherUser2', room: room)

    room.messages.create(text: 'Message1', user: user)
    room.messages.create(text: 'Message2', user: other_user)
    room.save!
  end

  scenario 'user enters in #Room1' do
    destroy_user_session
    login_with 'MyUser1'

    expect(page).to have_content('Room1')
    click_link('Room1')

    expect(page).to have_content('Room1 - Conversación')
    expect(page).to have_content('Message1')
    expect(page).to have_content('Message2')
    expect(page).to have_content('MyUser1')
    expect(page).to have_content('OtherUser2')
  end
end
