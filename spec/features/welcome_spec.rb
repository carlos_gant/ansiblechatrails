# frozen_string_literal: true

require 'rails_helper'

feature 'Welcome message', type: :feature do
  context 'users visits /#/lobby', js: true do
    before do
      destroy_user_session
      login_with 'MyUser'
    end
    it 'sees the welcome message' do
      expect(page).to have_content('Bienvenido, MyUser')
    end
  end
end
