# frozen_string_literal: true

require 'rails_helper'

feature 'Lobby', type: :feature, js: true do
  describe 'when user not logged in' do
    it 'redirects to /' do
      destroy_user_session
      visit '/#/lobby'
      expect(page).to have_content('Elige un nombre')
    end
  end
end
