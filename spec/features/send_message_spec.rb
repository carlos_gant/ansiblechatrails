# frozen_string_literal: true

require 'rails_helper'

feature 'Send message', type: :feature, js: true do
  background do
    room = Room.create!(name: 'Room1')

    user = User.create!(name: 'MyUser1')
    other_user = User.create!(name: 'OtherUser2')

    User.enter_room(user.id, room.id)
    User.enter_room(other_user.id, room.id)
  end

  scenario 'logged user enters in #Room1' do
    destroy_user_session
    login_with 'MyUser1'

    expect(page).to have_link('Room1')
    click_link('Room1')

    expect(page).to have_content('¿Cómo, no hay mensajes?')
    expect(page).to have_field('message')
    expect(page).to have_button('Enviar')

    fill_in('message', with: 'MyNewMessage1')
    click_button('Enviar')

    within('.comment') do
      expect(page).to have_content('MyUser1')
      expect(page).to have_content('MyNewMessage1')
    end
  end
end
