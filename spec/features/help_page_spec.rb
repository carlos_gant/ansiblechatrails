# frozen_string_literal: true

require 'rails_helper'

feature 'Help page', type: :feature do
  describe 'when user visits /#/', js: true do
    before do
      destroy_user_session
      visit root_path
    end
    it 'can see the help link' do
      expect(page).to have_link('Ayuda')
    end
    it 'should go to the about page when clicks the about button' do
      click_link('Ayuda')
      within('h2') do
        expect(page).to have_content('Ayuda')
      end
    end
  end
  describe 'when user visits /#/help directly', js: true do
    before do
      destroy_user_session
      visit '/#/help'
    end
    it 'show display the help page' do
      expect(page).to have_selector('h2', text: 'Ayuda')
      expect(page).to have_link('Volver')
    end
    it 'should go back when clicks the back button' do
      click_link('Volver')
      expect(page).to have_content('Elige un nombre')
    end
  end
end
