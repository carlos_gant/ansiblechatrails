# frozen_string_literal: true

require 'rails_helper'

feature 'Land page', type: :feature, js: true do
  describe 'when user not logged in' do
    before do
      destroy_user_session
      visit root_path
    end
    it 'must stay in /' do
      expect(page).not_to have_content('Salas de chat')
    end
    it 'displays the title' do
      expect(page).to have_content('ANSIBLE 1.0')
    end
    it 'displays a label on top of the user input field' do
      expect(page).to have_content('Elige un nombre')
    end
    it 'displays an user input field' do
      expect(page).to have_field('username')
    end
    it 'displays a random placeholder in the user input field' do
      expect(page).to have_selector('input[placeholder]')
    end
    it 'displays a submit button' do
      expect(page).to have_button('Entrar')
    end
    it 'redirects to lobby when user fills the form' do
      login_with 'MyUser'
      expect(page).to have_content('Salas de chat')
    end
  end
  describe 'when user is logged' do
    it 'should redirect to lobby' do
      destroy_user_session
      login_with 'MyUser'
      visit root_path
      expect(page).to have_content('Salas de chat')
    end
  end
end
