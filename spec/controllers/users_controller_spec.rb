# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe 'POST #create' do
    it 'creates a new user' do
      expect(User.count).to eq 0
      post :create, params: { name: 'user1' }
      expect(User.count).to eq 1
    end
    it 'reloads an existing users' do
      User.create!(name: 'user1')
      expect(User.count).to eq 1
      post :create, params: { name: 'user1' }
      expect(User.count).to eq 1
    end
  end
  describe 'POST #enter_room' do
    let(:user) { User.create!(name: 'User One') }
    let(:room) { Room.create!(name: 'My Room') }
    it 'sets the user active room' do
      post :enter_room, params: { room_id: room.id, user_id: user.id }
      room.reload
      expect(room.users).to include(user)
    end
  end
end
