# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ChatController, type: :controller do
  describe 'GET #index' do
    it 'renders the view :index' do
      get :index
      expect(response).to render_template(:index)
    end
  end
end
