# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MessagesController, type: :controller do
  let(:user) { User.create!(name: 'user') }
  let(:room) { Room.create!(name: 'Room1', users: [user]) }

  describe 'POST #create' do
    it 'should create a message in the room' do
      expect(room.messages.count).to eq(0)

      post :create, params: {
        user_id: user.id,
        room_id: room.id,
        text: 'HelloWorld!'
      }

      room.reload
      expect(room.messages.count).to eq(1)
    end
  end
end
