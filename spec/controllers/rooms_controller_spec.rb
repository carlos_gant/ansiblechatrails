# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RoomsController, type: :controller do
  context 'empty database' do
    describe 'GET #index' do
      before do
        get :index
      end
      it_behaves_like 'a json response'
      it 'should respond with an empty room list' do
        expect(response.body).to have_json_size(0).at_path('rooms')
      end
    end

    describe 'POST #create' do
      let(:room) { Room.new(name: 'My Room') }
      before do
        post :create, params: { name: room.name }
      end
      it_behaves_like 'a json response'
      it 'should respond with the created room' do
        expect(response.body).to be_json_eql(room.to_json).at_path('room')
      end
    end
  end

  context 'with some rooms' do
    before do
      @room = Room.create!(name: 'My Room')
    end

    describe 'GET #index' do
      before { get :index }
      it_behaves_like 'a json response'
      it 'should respond with a list of rooms' do
        expect(response.body).to have_json_size(1).at_path('rooms')
      end
    end

    describe 'GET #show' do
      before do
        get :show, params: { id: @room.id }
      end
      it_behaves_like 'a json response'
      it 'should respond room' do
        expect(response.body).to be_json_eql(@room.to_json).at_path('room')
      end
    end
  end
end
