# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { User.create!(name: 'MyUser') }
  let(:room) { Room.create!(name: 'MyRoom') }

  describe 'model' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe '.name' do
    it { is_expected.to have_field(:name).of_type(String) }
    it { is_expected.to have_index_for(name: 1) }
    it { is_expected.to validate_presence_of(:name) }

    it 'does require a name' do
      user.name = nil
      expect(user).not_to be_valid
    end

    it 'does require non empty name' do
      user.name = ''
      expect(user).not_to be_valid
    end

    it 'allow a name with spaces' do
      user.name = 'user with spaces'
      expect(user).to be_valid
    end

    it 'does not allow weird characters' do
      user.name = 'Weird # Name'
      expect(user).not_to be_valid
    end
  end

  describe '.rooms' do
    it { is_expected.to belong_to(:room).of_type(Room) }
  end

  describe '.messages' do
    it { is_expected.to have_many(:messages).of_type(Message) }
  end
end
