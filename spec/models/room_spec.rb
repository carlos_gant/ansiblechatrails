# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Room, type: :model do
  let(:room) { Room.create(name: 'MyRoom') }

  describe 'model' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps }
  end

  describe '.name' do
    it { is_expected.to have_field(:name).of_type(String) }
    it { is_expected.to have_index_for(name: 1) }
    it { is_expected.to validate_presence_of(:name) }

    it 'does require a name' do
      room.name = nil
      expect(room).not_to be_valid
    end

    it 'does require non empty name' do
      room.name = ''
      expect(room).not_to be_valid
    end

    it 'allow a name with spaces' do
      room.name = 'room with spaces'
      expect(room).to be_valid
    end

    it 'does not allow weird characters' do
      room.name = 'Weird # Name'
      expect(room).not_to be_valid
    end
  end

  describe '.users' do
    it { is_expected.to have_many(:users).of_type(User) }
    it 'allow add users to a room' do
      room.users.create(name: 'User1')
      room.users.create(name: 'User2')
      expect(room).to be_valid
    end
  end

  describe '.messages' do
    it { is_expected.to have_many(:messages).of_type(Message) }
    it 'can have multiple messages' do
      user = User.create(name: 'User1')
      message1 = Message.create(text: 'msg1', user: user)
      message2 = Message.create(text: 'msg2', user: user)
      room.messages << message1
      room.messages << message2
      expect(room).to be_valid
    end
  end
end
