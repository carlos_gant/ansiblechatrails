# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Message, type: :model do
  let(:user) { User.create(name: 'User 1') }
  let(:room) { Room.create(name: 'Room 1') }
  let(:message) do
    Message.create(text: 'Hello world', user: user, room: room)
  end

  describe 'model' do
    it { is_expected.to be_mongoid_document }
    it { is_expected.to have_timestamps.for(:creating) }
  end

  describe '.text' do
    it { is_expected.to have_field(:text).of_type(String) }
    it { is_expected.to validate_presence_of(:text) }

    it 'message it is required' do
      message.text = nil
      expect(message).not_to be_valid
    end

    it 'does require non empty message' do
      message.text = ''
      expect(message).not_to be_valid
    end

    it 'allow a normal message' do
      message.text = 'normal message with spaces'
      expect(message).to be_valid
    end

    it 'does allow weird characters' do
      message.text = 'Weird # message with ? some strange chars áéóñÑ€'
      expect(message).to be_valid
    end
  end

  describe '.user' do
    it { is_expected.to validate_presence_of(:user) }
    it { is_expected.to belong_to(:user).of_type(User).with_index }
  end

  describe '.room' do
    it { is_expected.to validate_presence_of(:room) }
    it { is_expected.to belong_to(:room).of_type(Room).with_index }
  end

  describe '.last_messages' do
    it 'should display only the configured messages' do
      more_messages = Message.configured_history + 5
      more_messages.times do |n|
        Message.create!(text: "Message #{n}", user: user, room: room)
      end
      found = Message.last_messages(room.id).pluck(:id)
      expect(found.length).to eq(Message.configured_history)
    end
  end
end
