# frozen_string_literal: true

RSpec.shared_examples_for 'a json response' do
  it { expect(response.status).to eq(200) }
  it { expect(response.content_type).to eq('application/json') }
end
