# frozen_string_literal: true

module FeatureSpecHelper
  def login_with(username)
    visit root_path
    within('#login_form') do
      fill_in('username', with: username)
      click_button('Entrar')
    end
    visit '/#/lobby'
  end

  # La sesion se mantiene en el lado de cliente (con sessionStorage)
  def destroy_user_session
    visit root_path
    page.execute_script('sessionStorage.clear()')
    Capybara.reset_sessions!
  end
end
